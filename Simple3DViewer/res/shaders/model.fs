#version 330 core
out vec4 FragColor;

in vec3 FragPosition;
in vec3 Normal;
in vec3 Color;
in vec2 TexCoord0;
in vec2 TexCoord1;
in vec2 TexCoord2;
in vec2 TexCoord3;
in vec2 TexCoord4;

uniform sampler2D diffuseTexture1;
uniform sampler2D specularTexture1;
uniform sampler2D emissiveTexture1;

uniform vec3 cameraPosition;
uniform vec3 lightPosition;

uniform vec3 ambientColor;
uniform vec3 diffuseColor;
uniform vec3 specularColor;
uniform vec3 emissiveColor;
uniform int diffuseTexturesCount;
uniform int emissiveTexturesCount;
uniform int diffuseUVChannel1;
uniform int emissiveUVChannel1;
uniform int specularUVChannel1;

void main()
{    
  vec4 baseColor = vec4(emissiveColor, 1.0);
  if (diffuseTexturesCount > 0)
  {
    vec4 diffuseSample;
    if(diffuseUVChannel1 == 0) diffuseSample = texture(diffuseTexture1, TexCoord0);
    else if(diffuseUVChannel1 == 1) diffuseSample = texture(diffuseTexture1, TexCoord1);
    else if(diffuseUVChannel1 == 2) diffuseSample = texture(diffuseTexture1, TexCoord2);
    else if(diffuseUVChannel1 == 3) diffuseSample = texture(diffuseTexture1, TexCoord3);
    else if(diffuseUVChannel1 == 4) diffuseSample = texture(diffuseTexture1, TexCoord4);
    baseColor = baseColor + diffuseSample;
  }
  if (emissiveTexturesCount > 0)
  {
    vec4 emissiveSample;
    if(emissiveUVChannel1 == 0) emissiveSample = texture(emissiveTexture1, TexCoord0);
    else if(emissiveUVChannel1 == 1) emissiveSample = texture(emissiveTexture1, TexCoord1);
    else if(emissiveUVChannel1 == 2) emissiveSample = texture(emissiveTexture1, TexCoord2);
    else if(emissiveUVChannel1 == 3) emissiveSample = texture(emissiveTexture1, TexCoord3);
    else if(emissiveUVChannel1 == 4) emissiveSample = texture(emissiveTexture1, TexCoord4);
    baseColor = baseColor + emissiveSample;
  }
  FragColor = baseColor;
}