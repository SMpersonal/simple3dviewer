#pragma once

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <algorithm>
#include <string>
#include <vector>

namespace Simple3D {
class ImGuiWrapper {
public:
  struct Slider {
    std::string text;
    float defaultValue;
    float currentValue;
    float minValue;
    float maxValue;
  };
  struct Checkbox {
    std::string text;
    bool value;
  };
  enum class Event {
    PostprocessesControlsChange,
    LightingControlsChange,
    VisualizeLightPositionCheckboxChange,
    ModelControlsChange,
    ModelLoadingConfigurationChange,
    CameraControlsChange,
    LoadModel,
    ReloadProgram,
  };
  class Mediator {
  public:
    virtual ~Mediator() = default;

    virtual void notify(Event e) = 0;
  };

  ImGuiWrapper(GLFWwindow *window,
               const std::vector<std::string> &postprocesses);
  ~ImGuiWrapper() { release(); }

  void update();
  void render() {
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
  }
  void release() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
  }
  void setMediator(Mediator *mediator);

public:
  const std::string &getSelectedFilePath() const { return filePath_; }
  const std::vector<Checkbox> &getPostprocessCheckboxes() const {
    return postprocessCheckboxes_;
  }
  const std::vector<Checkbox> &getLightControlCheckboxes() const {
    return lightControlCheckboxes_;
  }
  const std::vector<Checkbox> &getModelLoadingConfigurationCheckboxes() const {
    return modelLoadingConfigurationCheckboxes_;
  }
  const std::vector<Slider> &getLightControlSliders() const {
    return lightControlSliders_;
  }
  const std::vector<Slider> &getModelControlSliders() const {
    return modelTransformSliders_;
  }
  const std::vector<Slider> &getCameraControlSliders() const {
    return cameraControlSliders_;
  }
  void printError(const std::string &errorMessage) {
    cachedErrorMessage_ = errorMessage;
    ImGui::OpenPopup("errorPopup");
  }

private:
  Mediator *mediator_{nullptr};
  std::string filePath_;
  std::vector<Checkbox> postprocessCheckboxes_;
  std::vector<Checkbox> lightControlCheckboxes_;
  std::vector<Checkbox> modelLoadingConfigurationCheckboxes_;
  std::vector<Slider> lightControlSliders_;
  std::vector<Slider> modelTransformSliders_;
  std::vector<Slider> cameraControlSliders_;
  std::string cachedErrorMessage_;

  void drawSettingsWindow();
  void drawErrorPopup();
  void drawMainArea();
  void drawPostprocessesArea();
  void drawLightingArea();
  void drawModelArea();
  void drawCameraArea();
  void loadModelDialog();
};

} // namespace Simple3D
