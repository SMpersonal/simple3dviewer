#include <simple_3d_viewer/opengl/TextureCubeMap.hpp>

#include <cassert>
#include <iostream>

namespace Simple3D {
static constexpr int cubeMapImagesCount = 6;

static GLuint createTexture(const std::vector<ImageData> &images);

TextureCubeMap::TextureCubeMap(const std::vector<ImageData> &images) {
  texture_ = createTexture(images);
}

void TextureCubeMap::use(uint32_t textureSlot) const {
  glActiveTexture(GL_TEXTURE0 + textureSlot);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture_);
}

void TextureCubeMap::release() {
  if (texture_) {
    glDeleteTextures(1, &texture_);
    texture_ = 0;
  }
}

static GLuint createTexture(const std::vector<ImageData> &images) {
  assert(images.size() == cubeMapImagesCount);

  GLuint texture{};
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

  for (int i = 0; i < cubeMapImagesCount; ++i) {
    auto &image = images[i];
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, image.width,
                 image.height, 0, GL_RGB, GL_UNSIGNED_BYTE, image.image.get());
  }

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  // TODO:
  // Look into disabling the cubemap by via glBind

  return texture;
}
} // namespace Simple3D
