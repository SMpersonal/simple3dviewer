#pragma once

#include <glad/glad.h>

#include <vector>

#include <simple_3d_viewer/utils/ImageData.hpp>

namespace Simple3D {
class TextureCubeMap {
public:
  TextureCubeMap(const std::vector<ImageData> &images);
  TextureCubeMap(const TextureCubeMap &) = delete;
  TextureCubeMap(TextureCubeMap &&other) noexcept { std::swap(*this, other); }
  TextureCubeMap &operator=(const TextureCubeMap &) = delete;
  TextureCubeMap &operator=(TextureCubeMap &&other) noexcept {
    if (this == &other) {
      return *this;
    }

    release();

    swap(*this, other);
    return *this;
  }
  ~TextureCubeMap() { release(); }

  friend void swap(TextureCubeMap &lhs, TextureCubeMap &rhs) {
    std::swap(lhs.texture_, rhs.texture_);
  }

  void use(uint32_t textureSlot = 0) const;
  void release();

private:
  GLuint texture_ = 0;
};
} // namespace Simple3D
