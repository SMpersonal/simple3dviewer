#pragma once

#include <glad/glad.h>

#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

#include <simple_3d_viewer/opengl/Program.hpp>
#include <simple_3d_viewer/opengl/Texture2D.hpp>
#include <simple_3d_viewer/utils/ImageData.hpp>

namespace Simple3D {
struct TextureData {
  enum class Type {
    Diffuse,
    Specular,
    Emissive,
    Normals,
    Metalness,
    DiffuseRoughness
  };

  Texture2D *texture;
  // TODO: Remove this once you start refactoring Material and Model
  int imageIndex;
  std::optional<int> uvChannel;
  Type type;
};
extern const std::unordered_map<TextureData::Type, std::string>
    textureTypeToString;

class Material {
public:
  Material(std::vector<TextureData> &&textures)
      : textures(std::move(textures)), id_(idGenerator_++) {}

  glm::vec3 ambientColor{0.f, 0.f, 0.f};
  glm::vec3 diffuseColor{0.f, 0.f, 0.f};
  glm::vec3 specularColor{0.f, 0.f, 0.f};
  glm::vec3 emissiveColor{0.f, 0.f, 0.f};
  float opacity{1.f};
  float shininess{0.f};
  float shininessStrength{1.f};
  std::vector<TextureData> textures;

  void use(Program &program);
  uint64_t getID() const { return id_; }

private:
  const uint64_t id_;

  static uint64_t idGenerator_;
};
} // namespace Simple3D
