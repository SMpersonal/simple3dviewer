#include <simple_3d_viewer/rendering/Scene.hpp>

#include <string>
#include <vector>

#include <simple_3d_viewer/utils/ImageData.hpp>

namespace Simple3D {
static std::string lightProgramFilename = "light";
static std::string skyboxProgramFilename = "skybox";
static std::vector<std::string> skyboxImagePaths = {
    "res/textures/skybox/right.jpg", "res/textures/skybox/left.jpg",
    "res/textures/skybox/top.jpg",   "res/textures/skybox/bottom.jpg",
    "res/textures/skybox/front.jpg", "res/textures/skybox/back.jpg"};

static std::vector<ImageData> generateSkyBoxImages();

Scene::Scene(Size framebufferSize)
    : camera(framebufferSize), light(generateSphereMesh(0.1f)),
      lightProgram(lightProgramFilename), skybox(generateSkyboxMesh()),
      skyboxProgram(skyboxProgramFilename),
      skyboxTexture(generateSkyBoxImages()),
      modelProgram(modelProgramFilename) {}

static std::vector<ImageData> generateSkyBoxImages() {
  std::vector<ImageData> images;
  for (const auto &path : skyboxImagePaths) {
    images.emplace_back(loadImage(path, false));
  }
  return images;
}
} // namespace Simple3D
