#pragma once

#include <string>

#include <simple_3d_viewer/utils/ScopeGuard.hpp>

namespace Simple3D {
struct FreeImageDestructor {
  void operator()(uint8_t *image);
};

struct ImageData {
  ScopeGuard<uint8_t, FreeImageDestructor> image;
  std::string path;
  int width;
  int height;
  int colorChannelCount;
};

ImageData loadImage(const std::string &filePath, bool flip);
} // namespace Simple3D
