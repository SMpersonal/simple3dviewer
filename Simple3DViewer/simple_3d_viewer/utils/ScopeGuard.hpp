#pragma once

#include <filesystem>
#include <fstream>
#include <string>

namespace Simple3D {
struct DefaultDestructor {
  template <typename T> void operator()(T *ptr) { delete ptr; }
};

template <typename T, typename DestructorImpl = DefaultDestructor>
class ScopeGuard {
public:
  explicit ScopeGuard(T *ptr) : ptr_(ptr) {}
  ~ScopeGuard() {
    DestructorImpl{}(ptr_);
    ptr_ = nullptr;
  }

  ScopeGuard(const ScopeGuard &) = delete;
  ScopeGuard &operator=(const ScopeGuard &) = delete;

  // Allow move operations
  ScopeGuard(ScopeGuard &&other) noexcept : ptr_(other.ptr_) {
    other.ptr_ = nullptr;
  }
  ScopeGuard &operator=(ScopeGuard &&other) noexcept {
    if (this == &other) {
      return *this;
    }
    DefaultDestructor{}(ptr_);
    ptr_ = other.ptr_;
    other.ptr_ = nullptr;
    return *this;
  }
  const T *get() const { return ptr_; }
  T *get() { return ptr_; }

private:
  T *ptr_;
};
} // namespace Simple3D