import os
import subprocess

def change_working_dir_to_script_location():
   current_script_location = os.path.dirname(os.path.realpath(__file__))  
   os.chdir(current_script_location)

def check_git_submodule():
    if not os.path.exists(os.path.join("vcpkg", ".git")):
        print("Initializing vcpkg as a git submodule...")
        subprocess.check_call(["git", "submodule", "update", "--init"])

def bootstrap_vcpkg():
    print("Bootstrapping vcpkg...")
    if os.name == "nt":  # for Windows
        subprocess.check_call([r".\vcpkg\bootstrap-vcpkg.bat"], shell=True)
    else:  # for Unix-like systems
        subprocess.check_call([r"./vcpkg/bootstrap-vcpkg.sh"])

def main():
    change_working_dir_to_script_location()
    check_git_submodule()
    bootstrap_vcpkg()
    print("Done!")

if __name__ == "__main__":
    main()